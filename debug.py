# -*- coding: utf-8 -*-

import pprint
import sys

import nocurses

def cdprint(*args, **kwargs):
	if 'file' not in kwargs: kwargs['file'] = sys.stderr
	nocurses.print(*args, **kwargs)

def dprint(*args, **kwargs):
	if 'file' not in kwargs: kwargs['file'] = sys.stderr
	print(*args, **kwargs)

def dpprint(*args, **kwargs):
	if 'stream' not in kwargs: kwargs['stream'] = sys.stderr
	pprint.pprint(*args, **kwargs)

