# -*- coding: utf-8 -*-

import codecs
import json
import warnings
import zipfile

import debug
import net

try: import site_config
except ImportError: pass

class Index:
	def __init__(self):
		pass

	def load(self, repo_url, filename):
		""" Initialize index from 'url', with 'filename' as cache (read/write). """

		index_url = repo_url.rstrip("/") + "/index-v1.jar"
		self.repo_url = repo_url
		self.idx = self._load(index_url, filename)
		self._reorganize_index()
		return self

	def load_from_file(self, filename):
		""" Initialize index from a valid index file. """

		self.idx = self._load_from_file(filename)
		self._reorganize_index()
		return self

	def _load(self, url, filename):
		""" Download and return the index, or load from cache if available.

		'filename' will be overwritten if it is not a valid index file. """

		try:
			return self._load_from_file(filename)
		except IOError as e:
			if e.errno != 2: raise # Ignore (only) "File not found"
		except zipfile.BadZipFile as e: # The last download probably aborted.
			pass # Ignore and continue to overwrite.

		self._download(url, filename)
		return self._load_from_file(filename)

	def _load_from_file(self, filename):
		""" Load and return index from a valid index file. """
		with open(filename, "rb") as f:
			with zipfile.ZipFile(f) as z:
				with codecs.getreader('utf-8')(z.open('index-v1.json')) as cz:
					j = json.load(cz)
					self.filename = filename
					return j

	def _download(self, url, filename):
		""" Download 'url' and save to 'filename'.

		Contents of 'filename' _will_ be overwritten at all times, even
		if the url is invalid or the site can't be contacted! """

		with open(filename, "wb") as f:
			#with net.urlopen(repo_url, None, float(900)) as stream:
			with net.urlopen(url) as stream:
				f.write(stream.read())

	def _reorganize_index(self):
		""" Reorganize the index data structure in-place """

		# dict_keys(['repo', 'requests', 'packages', 'apps'])

		apps = {}
		for app in self.idx['apps']:
			apps[app['packageName']] = app
			app['packages'] = self.idx['packages'][app['packageName']]

		for appid in self.idx['packages'].keys():
			if appid not in apps:
				warnings.warn("Warning! Package with no app!: {!r}".format(appid))

		self.idx['apps'] = apps
		del self.idx['packages']

	def get_highest_available_version_code(self, pkgid):
		packages = self.idx['apps'][pkgid]['packages']
		if not packages: return None

		vcode = int(packages[0]['versionCode'])
		for p in packages:
			vcode = max(vcode, int(p['versionCode']))

	def _get_app(self, app):
		""" Return an app dictionary for 'app'.

		Helper function for parameter polymorphism. If 'app' is a string,
		look it up in the index and return the associated app dictionary.

		If 'app' is a dictionary, assume it already is an app dictionary,
		and return it unmodified. """

		if isinstance(app, str):
			return self.idx['apps'][app]
		elif isinstance(app, dict):
			return app
		else:
			raise(TypeError("'app' must be 'str' or 'dict'"))

	def get_available_version_codes(self, app):
		packages = self._get_app(app)['packages']
		return tuple(sorted(p['versionCode'] for p in packages))

	def get_package_for_versioncode(self, app, versioncode):
		for pkg in self._get_app(app)['packages']:
			if pkg['versionCode'] == versioncode: return pkg
		raise KeyError("{{{!r}, {!r}}} not in index".format(pkgid, versioncode))

	def get_version_for_versioncode(self, app, versioncode):
		pkg = self.get_package_for_versioncode(app, versioncode)
		return pkg['versionName']


def get_latest_index(config):
	import os
	import pprint

	index = Index().load(config['repo_url'], config['index_file'])

	pretty_fn = config['pretty_file']
	if not os.path.exists(pretty_fn):
		with open(pretty_fn, "w") as f:
			pprint.pprint(index.idx, stream=f)

	return index


if __name__ == '__main__':
	from Config import Config
	config = Config().load("untracked/config.json")
	index = get_latest_index(config)
