# stdlib
import argparse
import os.path
import re
import textwrap
import time

# in-tree deps
import debug
from Report import Report


def parse_args():
	parser = argparse.ArgumentParser(description="compares two indexes and spits out information in a format suitable for easy inclusion in TWIF.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("old", help="Old index to compare.")
	parser.add_argument("new", help="New index to compare. Optional. If omitted, will use the latest, downloading it if necessary.", nargs='?')
	options = parser.parse_args()
	return options

def get_specified_index_or_latest(path_to_config, path_to_index=None):
	import Index

	if path_to_index is None:
		from Config import Config
		config = Config().load(path_to_config)
		return Index.get_latest_index(config)
	else:
		return Index.Index().load_from_file(path_to_index)

def join(seq, sep, lastsep):
	""" Join strings in "seq" together using "sep" as separator.
	    BUT use "lastsep" for the last one. """

	if len(seq) == 0: return ""
	if len(seq) == 1: return seq[0]
	return lastsep.join((sep.join(seq[:-1]), seq[-1]))

def capitalize(phrase):
	if len(phrase) < 2: return phrase.capitalize()
	return phrase[0].upper() + phrase[1:]

def decapitalize(phrase):
	if len(phrase) < 2: return phrase.lower()
	if phrase[:2].isupper(): return phrase
	return phrase[0].lower() + phrase[1:]

def summarize(news):
	r = []

	header_pattern = re.compile("^#+ +(.+)$")
	first = True

	for line in news:
		line = line.rstrip()
		m = header_pattern.search(line)
		if not m: continue

		s = m.group(1)
		if first:	s = capitalize(s)
		else:		s = decapitalize(s)
		first = False
		r.append(s)

	return join(r, ", ", " and ")

if __name__ == '__main__':
	opts = parse_args()
	debug.dprint(repr(opts))

	oldindex = get_specified_index_or_latest(None, opts.old)
	newindex = get_specified_index_or_latest("untracked/config.json", opts.new)

	report = Report(oldindex, newindex)
	removed = report.get_removed_apps()
	added = report.get_added_apps()
	updated = report.get_updated_apps()
	downgraded = report.get_downgraded_apps()
	beta_updated = report.get_beta_updated_apps()

	with open("untracked/news.md", "r") as f:
		news = f.read().splitlines(True)

	summary = summarize(news) or "**FIXME**: write a summary"

	params = {
		"editionnumber": int((time.time() - 1523923200) / 604800),
		"title": "**FIXME**: set a title ",
		"summary": summary,
		"author": "Coffee",
		"date": time.strftime("%Y-%m-%d"),
		"number_of_added_apps": len(added),
		"number_of_removed_apps": len(removed),
		"number_of_updated_apps": len(updated),
		"number_of_beta_updated_apps": len(beta_updated),
		"number_of_downgraded_apps": len(downgraded)
	}

	frontmatter = textwrap.dedent("""\
	---
	layout: post
	title: "TWIF {editionnumber}: {title}"
	edition: {editionnumber}
	author: "{author}"
	authorWebsite: "https://open.source.coffee"
	fdroid: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #0d47a1; font-style: normal; font-weight: bold;">F-Droid</em>'
	featuredv1: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.5ex; box-shadow: 0.1em 0.05em 0.1em rgba(0, 0, 0, 0.3); border-radius: 1em; color: black; background: linear-gradient(orange, yellow);">Featured</em>'
	featured: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: orange; font-style: normal; font-weight: bold;">Featured</em>'
	major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
	number_of_added_apps: {number_of_added_apps}
	number_of_removed_apps: {number_of_removed_apps}
	number_of_updated_apps: {number_of_updated_apps}
	number_of_beta_updated_apps: {number_of_beta_updated_apps}
	number_of_downgraded_apps: {number_of_downgraded_apps}
	mastodonAccount: "**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**"
	twifTag: "**[#TWIF](https://mastodon.technology/tags/twif)**"
	twifThread: "[TWIF submission thread](https://forum.f-droid.org/t/twif-submission-thread)"
	matrixRoom: "[#fdroid:f-droid.org](https://matrix.to/#/#fdroid:f-droid.org)"
	telegramRoom: "https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA"
	forum: "https://forum.f-droid.org"
	---
	""").strip().format_map(params)

	print(frontmatter)
	print()

	header = textwrap.dedent("""\
	This Week In F-Droid {{{{ page.edition }}}}, Week {{{{ page.date | date: "%V, %G" }}}} <a href="{{{{ site.baseurl }}}}/feed.xml"><img src="{{{{ site.baseurl }}}}/assets/Feed-icon-16x16.png" alt="Feed"></a>

	In this edition: {summary}.
	There are **{{{{ page.number_of_added_apps }}}}** new and **{{{{ page.number_of_updated_apps }}}}** updated apps.

	<!--more-->

	**[F-Droid](https://f-droid.org/)** is a [repository](https://f-droid.org/packages/) of verified [free and open source](https://en.wikipedia.org/wiki/Free_and_open-source_software) Android apps, a **[client](https://f-droid.org/app/org.fdroid.fdroid)** to access it, as well as a whole "app store kit", providing all the tools needed to set up and run an app store. It is a community-run free software project developed by a wide range of contributors. This is their story this past week.
	""").strip().format_map(params)

	print(header)
	print()

	print("".join(news))

	if downgraded:
		print("### !Downgraded apps!")
		print()
		print("".join(downgraded))
		print("{{ page.number_of_downgraded_apps }} apps were downgraded (wUt?!).")
		print()

	if added:
		print("### New apps")
		print()
		print("".join(added))

	if updated:
		print("### Updated apps")
		print()

		print("In total, **{{ page.number_of_updated_apps }}** apps were updated this week. Here are the highlights:")
		print()
		print("".join(updated))

	if beta_updated:
		print("### Beta updates")
		print()
		print("""The following updates won't be automatically suggested to you unless you have "Unstable updates" enabled in the F-Droid app settings, but you can expand the "Versions" tab and install them manually. Note that these are marked beta for a reason: proceed at your own risk.""")
		print()
		print("".join(beta_updated))
		print("{{ page.number_of_beta_updated_apps }} apps had beta upgrades.")
		print()

	if removed:
		print("### Removed apps")
		print()
		print("".join(removed))
		print("{{ page.number_of_removed_apps }} apps were removed.")
		print()

	footer = textwrap.dedent("""\
	### Tips and Feedback

	Do you have important app updates we should write about? Send in your tips via [Mastodon](https://joinmastodon.org)! Send them to {{{{ page.mastodonAccount }}}} and remember to tag with {{{{ page.twifTag }}}}. Or use the {{{{ page.twifThread }}}} on the forum. The deadline to the next TWIF is **Thursday** 12:00 UTC.

	General feedback can also be sent via Mastodon, or, if you'd like to have a live chat, you can find us in **#fdroid** on {{Freenode}}, on Matrix via {{{{ page.matrixRoom }}}} or on [Telegram]({{{{ page.telegramRoom }}}}). All of these spaces are bridged together, so the choice is yours. You can also join us on the **[forum]({{{{ page.forum }}}})**.
	""").strip().format_map(params)

	print(footer)
