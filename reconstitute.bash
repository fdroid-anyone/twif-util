#!/bin/bash

### Reconstitute a file from one original and a series of diffs,
### without generating any intermediate files.
###
### The arguments to the script should be the diffs to apply, in order.
### The name of the original file is included in the first diff, and
### therefore doesn't need to be specified. Similarly, the output filename
### is given by the last diff.
###
###    ### Given this, do not run this program on untrusted files! ###
###
### For bonus points, the md5sum is extracted from the filename to verify
### that the file was rebuilt correctly.

eecho ()
{
	echo "$@" 1>&2
}

failed ()
{
	eecho "Failed" "$@"
	exit 1
}

try ()
{
	eecho "> $*"
	"$@" || failed to execute \""$@"\"
}

AWK_GET_MD5='
	BEGIN {
		for (x=0; x < 32; x++) p = p "[0-9a-f]"
		p = "\\." p "\\."
	}
	{
		if (match($0, p)) print substr($0, RSTART+1, RLENGTH-2)
	}
'

export DIFFS=( "$@" )

(
	LASTLINE=

	for F in "${DIFFS[@]}"; do
		eecho "$F"
		if [ -z "$LASTLINE" ]; then
			< "$F" xz -dc | head -n -1
		else
			< "$F" xz -dc | head -n -1 | tail -n +2
		fi
		LASTLINE="$( < "$F" xz -dc | tail -n 1 )"
	done

	if [ -n "$LASTLINE" ]; then
		echo "$LASTLINE"
	fi
) | ed -r

LASTDIFF="${DIFFS[-1]}"
LASTLINE="$( < "$LASTDIFF" xz -dc | tail -n 1 )"
O="${LASTLINE:2}"
[ -e "$O" ] || Failed to extract \""$O"\"
eecho Extracted to \""$O"\"

MD5="$( md5sum "$O" | awk '{print $1}' )"
MD52="$( echo "$LASTDIFF" | awk "$AWK_GET_MD5" )"

[ -n "$MD5" ] || Failed to calculate md5sum for \""$O"\"
[ -n "$MD52" ] || failed to extract md5sum from filename \""$LASTDIFF"\"
[ "$MD5" = "$MD52" ] || Failed to correctly extract \""$O"\": checksum differs!

eecho Checksums match! You win!

exit 0
